package Date;

public class Date {
    private int fecha;//Dato que almacenara el año, mes y día en bits

    public Date(){
        fecha=0;
    }
    //Devuelve el año moviendo los bits al final
    public int getAño(){
        int año = fecha >>> 20;
        return año;
    }
    //Devuelve el mes moviendo los bits al inicio para no contar los bits del año
    //Luego los devuelve al final para entregar el mes
    public int getMes(){
        int mes = (fecha << 12) >>> 28;
        return mes;
    }
    //Devuelve el dia moviendo los bits al inicio para no contar los bits del año
    //Luego los devuelve al final para entregar el dia
    public int getDia(){
        int dia = (fecha << 16) >>> 27;
        return dia;
    }
    //Ingresa el año para la fecha
    public void setAño(int año){
        //Revisa si el valor del año es posible segun la cantidad de bits disponibles
        if(año>=0 && año<4096){
            //Coloca los datos binarios del año en la fecha, eliminando la anterior usando una mascara
            int mask=1048575;//00000000000011111111111111111111
            fecha = (fecha & mask) | (año << 20);
            //System.out.println(Long.toBinaryString(fecha));
        }else{
            //Sino esta en el rango respondera el error
            System.out.println("Año no reconocible");
        }
    }
    //Cambia el mes de la fecha
    public void setMes(int mes){
        //Revisa si el valor del mes es posible segun la cantidad de bits disponibles
        if(mes>=1 && mes<13){
            //Agrega los bits del mes en la fecha, borrando la anterior usando una mascara
            int mask=-983041;//11111111111100001111111111111111
            fecha = (fecha & mask) | (mes << 16);
            //System.out.println(Long.toBinaryString(fecha));
        }else{
            //Si el mes supera el tamaño conocido, este no es correcto
            System.out.println("Mes no valido");
        }
    }
    //Cambia el dia en la fecha registrada
    public void setDia(int dia){
        //Analiza si el dia ingresado es posible acorde a la cantidad de bits
        if(dia>=1 && dia<32){
            //Ingresa los bits en la fecha, eliminando la anterior agregada
            //A traves de una mascara
            int mask =-63489;//11111111111111110000011111111111
            fecha = (fecha & mask) | (dia << 11);
            //System.out.println(Long.toBinaryString(fecha));
        }else{
            //Si el dia no es parte del mes, no coincide
            System.out.println("Dia no valido");
        }

    }
    //Imprime la fecha en formato String, como DD/MM/YYYY
    @Override
    public String toString() {
        if(getMes()<10){
            return getDia() + "/0" + getMes() + "/" + getAño();
        }else{
            return getDia() + "/" + getMes() + "/" + getAño();
        }
    }
    //.
}
