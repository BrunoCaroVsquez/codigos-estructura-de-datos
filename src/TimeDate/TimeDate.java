package TimeDate;

public class TimeDate {
    private int fecha;

    public TimeDate(){
        fecha=0;
    }

    public int getAño(){
        int año = fecha >>> 20;
        return año;
    }

    public int getMes(){
        int mes = (fecha << 12) >>> 28;
        return mes;
    }

    public int getDia(){
        int dia = (fecha << 16) >>> 27;
        return dia;
    }

    public int getHora(){
        int hora = (fecha << 21) >>> 27;
        return hora;
    }
    public int getMinuto(){
        int minuto = (fecha << 26) >>> 26;
        return minuto;
    }

    public void setAño(int año){
        if(año>=0 && año<4096){
            int mask=1048575;//00000000000011111111111111111111
            fecha = (fecha & mask) | (año << 20);
            //System.out.println(Long.toBinaryString(fecha));
        }else{
            System.out.println("Año no reconocible");
        }
    }

    public void setMes(int mes){
        if(mes>=1 && mes<13){
            int mask = -983041;//11111111111100001111111111111111
            fecha = (fecha & mask) | (mes << 16);
            //System.out.println(Long.toBinaryString(fecha));
        }else{
            System.out.println("Mes no valido");
        }
    }

    public void setDia(int dia){
        if(dia>=1 && dia<32){
            int mask = -63489;//11111111111111110000011111111111
            fecha = (fecha & mask) | (dia << 11);
            //System.out.println(Long.toBinaryString(fecha));
        }else{
            System.out.println("Dia no valido");
        }

    }

    public void setHora(int hora){
        if(hora>=0 && hora<24){
            int mask = -1985;//11111111111111111111100000111111
            fecha = (fecha & mask) | (hora << 6);
            //System.out.println(Long.toBinaryString(fecha));
        }else{
            System.out.println("Hora no valida");
        }
    }

    public void setMinuto(int minuto){
        if(minuto>=0 && minuto<59){
            int mask = -64;//11111111111111111111111111000000
            fecha = (fecha & mask) | minuto;
            //System.out.println(Long.toBinaryString(fecha));
        }else{
            System.out.println("Minuto no valido");
        }
    }

    public String toString() {
        String resp = "";
        if (getDia() < 10) {
            resp += "0" + getDia() + "/";
        } else {
            resp += getDia() + "/";
        }
        if (getMes() < 10) {
            resp += "0" + getMes() + "/" + getAño();
        } else {
            resp += getMes() + "/" + getAño();
        }
        if (getHora() < 10) {
            resp += " 0" + getHora() + ":";
        } else {
            resp += " " + getHora() + ":";
        }
        if(getMinuto() < 10){
            resp+="0" + getMinuto();
        }else{
            resp+=getMinuto();
        }

        return resp;
    }

    public boolean sameDate(TimeDate t){
        int fecha, fecha2;
        fecha = (getAño() << 9) | (getMes() << 5) | (getDia());
        fecha2 = (t.getAño() << 9) | (t.getMes() << 5) | (t.getDia());
        //System.out.println(Long.toBinaryString(fecha2));
        //System.out.println(Long.toBinaryString(fecha));
        return fecha == fecha2;
    }

    public boolean isBefore(TimeDate t){
        int fecha, fecha2;
        fecha = (getAño() << 9) | (getMes() << 5) | (getDia());
        fecha2 = (t.getAño() << 9) | (t.getMes() << 5) | (t.getDia());
        //System.out.println(Long.toBinaryString(fecha2));
        //System.out.println(Long.toBinaryString(fecha));
        return fecha < fecha2;
    }

    public boolean isAfter(TimeDate t){
        int fecha, fecha2;
        fecha = (getAño() << 9) | (getMes() << 5) | (getDia());
        fecha2 = (t.getAño() << 9) | (t.getMes() << 5) | (t.getDia());
        //System.out.println(Long.toBinaryString(fecha2));
        //System.out.println(Long.toBinaryString(fecha));
        return fecha > fecha2;
    }
}
