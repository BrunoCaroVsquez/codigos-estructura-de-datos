package ArbolDeExpresion;

import java.util.Queue;
import java.util.Stack;

/**
 * 
 * @author Martita
 * 
 * Para m�s info, visitar https://es.stackoverflow.com/questions/26907/infijo-a-posfijo-en-java
 * Y http://informatica.uv.es/iiguia/AED/laboratorio/P6/pr_06_2005.html
 *
 */

public class Parsing {
	/*Método pasa el String del proceso de un orden infijo a uno postfijo
	con la ayuda de una pila*/
	public static String fromInfijoToPostfijo(String infijo) {
		//Establece un String de resultado y una pila de caracteres del proceso
		String result = "";
		Stack<Character>Pila = new Stack<>();
		//Crea un arreglo de caracteres para almacenar cada valor del String como tokens.
		char[] tokens = infijo.toCharArray();
		//Para cada token revisará el valor de cada uno según su prioridad
		for(char x: tokens){
			//Si el digito es un numero, se agregara al String de resultado
			if(isANumber(x)){
				result+=x;
				//Si el digito es un paréntesis abierto, lo guardara en la pila
			}else if(prioridad(x)==0){
				Pila.push(x);
				/*Si es un paréntesis cerrado, va a liberar los valores de la pila
				y agregarlos en el String hasta que el tope sea un paréntesis cerrado
				 */
			}else if(prioridad(x)==-1){
				while(Pila.peek()!='('){
					result+=Pila.pop();
				}
				Pila.pop();
				/*Si el dígito es un operador, comprobara si la pila esta vacía
				para insertar el operador, en caso contrario, revisará según si la
				prioridad del dígito es mayor que la del tope de la pila, para
				almacenar dicho dígito en la pila, o si es menor, agregar el tope de
				la pila en el String y empilar el token en la pila
				 */
			}else if(prioridad(x)==1 || prioridad(x)==2){
				if(Pila.isEmpty()){
					Pila.push(x);
				}else{
					if(prioridad(x)>prioridad(Pila.peek())){
						Pila.push(x);
					}else{
						result+=Pila.pop();
						Pila.push(x);
					}
				}
			}
		}
		/*Finalmente agrega al String cualquier valor aun almacenado en la pila
		y retorna dicho resultado.
		 */
		while(!Pila.isEmpty()){
			result+=Pila.pop();
		}
		return result;
	}
	
	private static boolean isANumber(char symbol) {
		return symbol == '0' || symbol == '1' || symbol == '2' || symbol == '3' || 
				symbol == '4' || symbol == '5' || symbol == '6' || symbol == '7' || 
				symbol == '8' || symbol == '9';
	}

	private static int prioridad(char symbol) {
		if(symbol == '(') return 0;
		if(symbol == '+' || symbol == '-') return 1;
		if(symbol == '*' || symbol == '/') return 2;
		return -1;
	}

	/*Método sirve para construir el árbol expresión dado de forma postfija
	mediante el uso de una pila y un árbol binario
	 */
	public static ArbolExpresion getArbol(String postfijo) {
		//Establece una pila de árbol expresión para almacenarlo
		Stack<ArbolExpresion>Pila = new Stack<>();
		//Crea un arreglo para almacenar los caracteres del string como tokens
		char[] tokens = postfijo.toCharArray();
		/*Revisa cada tokens si este es un número u otro, para poder empilarlo
		u en otro caso liberar los dos primeros elementos de la pila y asignarlos
		como hijos izquierdo y derecho del árbol.
		 */
		for(char s: tokens){
			ArbolExpresion A = new ArbolExpresion(s);
			if(isANumber(s)){
				Pila.push(A);
			}else{
				A.left=Pila.pop();
				A.right=Pila.pop();
				Pila.push(A);
			}
		}
		//Al final del proceso, retornara el tope de la pila
		return Pila.peek();
	}
	//Método entrega el resultado del árbol expresión como un valor int
	public static int Calculadora(ArbolExpresion x){
		if(x==null){
			return 0;
		}
		/*Establece dos int valor, iguales al método de calculadora usando el nodo
		derecho del árbol, y result.
		 */
		int valor = Calculadora(x.right);
		int result = 0;
		//Si el symbol del nodo es un número, result será el valor numérico del carácter
		if(isANumber(x.symbol)){
			result = Character.getNumericValue(x.symbol);
			/*Si la prioridad es un operador, result será el dato de valor
			sumado, restado, multiplicado o dividido por el proceso de recursividad
			del hijo izquierdo.
			 */
		}else if(prioridad(x.symbol) == 1 || prioridad(x.symbol) == 2){
			switch (x.symbol){
				case '+': result = valor + Calculadora(x.left);break;
				case '-': result = valor - Calculadora(x.left);break;
				case '*': result = valor * Calculadora(x.left);break;
				case '/': result = valor / Calculadora(x.left);break;
			}
		}
		//Final retornará el resultado.
		return result;
	}
}
