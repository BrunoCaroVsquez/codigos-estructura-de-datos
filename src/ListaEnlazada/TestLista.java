package ListaEnlazada;

import java.util.Scanner;

public class TestLista {
    public static void main(String [] args) {
        Lista l = new Lista();
        Scanner sc = new Scanner(System.in);
        int num, data;
        System.out.println("Ingrese la cantidad de datos a obtener");
        num=10;
        /*
        System.out.println("Ingrese los datos a continuacion");
        for(int i = 0; i<num; i++){
            data = sc.nextInt();
            if (i % 2 == 1) {
                l.InsertaInicio(data);
            } else {
                l.InsertaFinal(data);
            }
        }

         */
        l.InsertaFinal(5);
        l.InsertaInicio(16);
        l.InsertaFinal(29);
        l.InsertaInicio(36);
        l.InsertaFinal(49);
        l.InsertaInicio(100);
        l.InsertaFinal(1);
        l.InsertaInicio(78);
        l.InsertaFinal(61);
        l.InsertaInicio(23);
        System.out.println("La lista posee los siguientes datos:");
        l.Print();
        int prom = l.Promedio();
        System.out.println("\nPromedio de la lista es:" + prom);
        Lista x = l.GetMayoresProm(prom);
        System.out.println("Los datos mayores al promedio son:");
        x.Print();
        /*
        System.out.println("l Esta Vacia?: " + l.EstaVacia());
        System.out.println("Size de l: " + l.Size());
        l.InsertaInicio(5);
        System.out.println("l Esta Vacia?: " + l.EstaVacia());
        System.out.println("Size de l: " + l.Size());
        l.Print();
        l.InsertaInicio(100);
        System.out.println("\nSize de l: " + l.Size());
        l.Print();
        l.InsertaInicio(43);
        System.out.println("\nSize de l: " + l.Size());
        l.Print();
        System.out.println("\nSize de l: " + l.Size());
        System.out.println("l Esta Vacia?: " + l.EstaVacia());
        l.InsertaInicio(30);
        l.InsertaInicio(29);
        l.InsertaInicio(54);
        l.InsertaInicio(32);
        l.InsertaInicio(2);
        l.Print();
        int num = l.Promedio();
        System.out.println("\nPromedio de l:" + num);
        Lista x = l.GetMayoresProm(num);

        System.out.println("x Esta Vacia?: " + x.EstaVacia());
        System.out.println("Size de x: " + x.Size());
        x.Print();
        */
    }
}
