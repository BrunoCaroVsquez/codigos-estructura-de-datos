package PilayCola;

import java.util.LinkedList;
import java.util.Queue;
//Crea una clase para contar el numero de llamadas
//recientes en cierto tiempo
public class RecentCounter {
    Queue<Integer> Counter;//Cola para almacenar las llamadas
    public RecentCounter() {
        Counter = new LinkedList<>();

    }
    //Añade una llamada a la cola y retorna las ocurridas en
    //los ultimos 3000 milisegundos
    public int ping(int t) {
        Counter.add(t);
        while (Counter.peek() < t - 3000) {
            Counter.poll(); }
        return Counter.size();
    }
}