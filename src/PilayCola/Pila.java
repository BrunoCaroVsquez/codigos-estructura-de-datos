package PilayCola;

public class Pila{
    int top;
    int tam;
    char [] data;

    public Pila(int tam){
        this.tam=tam;
        data=new char [tam];
        top = -1;
    }

    public void Push(char x){
        data[++top]=x;
    }

    public char Pop(){
        return data[top--];
    }

    public char Top(){
        return data[top];
    }


}

class Solution {
    //Metodo permite para calcular el tamaño de los parentesis
    //Siempre y cuando estos sean un String de parentesis valido
    public int maxDepth(String s) {
        Pila p = new Pila(s.length());//Pila para usar de contador
        //Agregara los caracteres de la pila en orden inverso
        for (int i = s.length() - 1; i >= 0; i--) {
            p.Push(s.charAt(i));
        }
        //Declara valor de profundidad resp y el conteo de profundidad actual
        int resp = 0, cant = 0;
        for (int i = 0; i < s.length(); i++) {
            //La iteracion aumentara la profundidad del momento al encontrar un "("
            //Y la reducira si encuentra un ")"
            if (p.Top() == '(') {
                cant++;

            } else if (p.Top() == ')') {
                cant--;

            }
            //Al final guardara en la respuesta el valor mas alto entre si mismo
            //Y la cantidad actual
            resp = Math.max(resp, cant);
            //Elimina el dato superior de la pila
            p.Pop();
        }
        return resp;
    }

}

