package MaxHeap;

import java.util.Arrays;

public class MaxHeap {

	private int length = 10;
	private int size = 0;
	private int [] items = new int[length];
	
	public MaxHeap() {
		
	}
	
	public MaxHeap(int [] A) {
		// Build heap
		items = A;
		size = A.length;
		length = A.length;
		buildHeap();
		
		duplicateCapacity();
	}
	
	public void printHeap() {
		for(int i = 0; i< size; i++) {
			System.out.print(items[i] + " ");
		}
		System.out.println();
	}
	
	public void insert(int item) {
		// insert a new element
		duplicateCapacity();
		items[size] = item;
		size++;
		heapifyUp(size-1);
	}
	
	public int top() {
		// get items[0]
		if(size > 0)	return items[0];
		else return -1;
	}
	
	public int delete() {
		// get items[0] and delete it
		if(size > 0) {
			int item = items[0];
			items[0] = items[size-1];
			size--;
			heapify(0);
			return item;
		}
		return -1;
	}
	
	private void duplicateCapacity(){
		// duplicate the capacity of the array
		if(size == length) {
			items = Arrays.copyOf(items, length*2);
			length *= 2;
		}
	}
	/*Método ajusta el nodo del heap en su posición i
	ayudando a subir el árbol mientras el valor sea mayor que su padre
	 */
	private void heapifyUp(int i) {
		//Establece un int p, del valor del padre la posición i
		int p = parent(i);
		/*Si el valor en i es mayor al de posición p, se cambiaran
		de lugar ambos valores y se continuara el método con el valor p
		 */
		if(items[i]>items[p]){
			swap(i,p);
			heapifyUp(p);
		}
	}

	private void heapify(int i) {
		if(isLeaf(i)){
			return;
		}
		int l= leftChild(i);
		int r= rightChild(i);
		if(items[i]<items[l] || items[i]<items[r]){
			int largest;
			if(l<= size && items[l] > items[i]){
				largest = l;
			}else{
				largest = i;
			}
			if(r<= size && items[r] > items[largest]){
				largest = r;
			}
			if(largest != i){
				swap(i, largest);
			}
			heapify(largest);
		}
	}

	private boolean isLeaf(int pos){
		if(pos>(size/2) && pos <= size){
			return true;
		}
		return false;
	}
	
	private void buildHeap() {
		// Given an array, get the heap
		for(int i = size/2; i >= 0; i--) {
			heapify(i);
		}
	}
	
	private int parent(int i) {
		return (i-1)/2;
	}
	private int leftChild(int i) {
		return 2*i+1;
	}
	private int rightChild(int i) {
		return 2*i+2;
	}
	private void swap(int i, int j) {
		int temp = items[i];
		items[i] = items[j];
		items[j] = temp;
	}

	public static void main(String[] args) {
		System.out.println("El heap es:");
		MaxHeap a = new MaxHeap();
		a.insert(4);
		a.insert(8);
		a.insert(9);
		a.insert(12);
		a.insert(4);
		a.insert(20);
		a.insert(35);
		a.insert(5);
		a.insert(53);
		a.insert(99);
		a.buildHeap();
		a.printHeap();
	}
}
