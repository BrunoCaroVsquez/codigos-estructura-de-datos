package Grafos;

public class StarGraph {
    /*
    El metodo revisara ambos lados de las primeras dos filas de la matriz en busca de revisar
    si estos poseen un valor en comun entre sus columnas, si esto ocurre, entonces retornara
    el valor comun, sino, retornara -1.
     */
    public int findCenter(int[][] edges) {
        if(edges[1][0] == edges[0][0] || edges[1][0] == edges[0][1])
            return edges[1][0];
        else if(edges[1][1] == edges[0][0] || edges[1][1] == edges[0][1])
            return edges[1][1];

        return -1;
    }
}
