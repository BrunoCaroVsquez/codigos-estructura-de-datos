package Grafos;

import java.util.*;

public class ValidPath {
    /*
    El método hace una búsqueda en anchura del grafo, usando una cola q, para almacenar los nodos del grafo
    que sean visitados y extraerlos para continuar el proceso de búsqueda hasta encontrar el nodo destino.
    También posee un arreglo de boolean para marcar los nodos que ya hayan sido visitados, para finalmente retornar
    el valor de destino si es que q lo contiene al final del método, y sino lo contiene retornará -1.
     */
    public boolean validPath(int n, int[][] edges, int source, int destination) {
        //Comprueba si la longitud de la matriz es 0
        if(edges.length == 0){
            return true;
        }
        //Crea una cola y arreglo de boolean para hacer una búsqueda en amplitud
        Queue<Integer> q = new LinkedList<>();
        boolean[] visited = new boolean[n];
        q.add(source);
        //Proceso de búsqueda en amplitud
        while (!q.isEmpty()) {
            int t = q.poll();
            //Comprueba por cada fila de la matriz si el valor izquierdo es t y
            //el boolean en la posición del valor derecho de la fila i es false
            //agrega en la cola el valor derecho de la fila y dicha posición
            //se vuelve true
            for (int i = 0; i < edges.length; i++) {
                if (edges[i][0] == t && visited[edges[i][1]]==false) {
                    q.add(edges[i][1]);
                    visited[edges[i][1]]=true;
                }
                //Comprueba si el valor derecho de la fila i es t y
                //el boolean en la posición del valor izquierdo de la fila i es false
                //agrega en la cola el valor izquierdo de la fila y dicha posición
                //se vuelve true
                if (edges[i][1] == t && visited[edges[i][0]]==false) {
                    q.add(edges[i][0]);
                    visited[edges[i][0]]=true;
                }
            }
            //Si la cola contiene el destino, es retorna true, sino retorna false.
            if (q.contains(destination)){
                return true;
            }
        }
        return false;
    }
}
