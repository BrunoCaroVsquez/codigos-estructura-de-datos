package Grafos;

public class TownJudge {
    /*
    El método creara un arreglo de tamaño n con la cual registrara que dato le entrega su confianza a otro.
    Haciendo que por un ciclo reduzca el valor del dato en la posición del arreglo que se sitúe a la izquierda de la
    matriz trust y aumentara el valor del dato que este a su derecha, para finalmente ubicar que dato en el arreglo posee
    un valor igual a n-1, retornando la posición de este más 1, sino encuentra ninguno que tenga esa categoria, retornara
    -1.
     */
    public int findJudge(int n, int[][] trust) {
        int[] confianzas = new int[n];
        //Reduce la posición del valor izquierdo en 1 y aumenta el del valor derecho en 1
        //por cada fila de la matriz trust
        for(int i=0;i<trust.length;i++){
            confianzas[trust[i][0]-1]--;
            confianzas[trust[i][1]-1]++;
        }
        //Buscará en cada dato del arreglo si este contiene el valor n-1, en cuyo caso, retorna
        //la posición i+1
        for(int i=0;i<n;i++){
            if(confianzas[i]==(n-1))
                return i+1;
        }
        //Si no encuentra el valor, retornara -1
        return -1;
    }


}
