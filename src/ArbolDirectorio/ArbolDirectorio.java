package ArbolDirectorio;

import java.util.LinkedList;
import java.util.StringTokenizer;

public class ArbolDirectorio {

    private class Carpeta{
        String nombre;
        LinkedList<Carpeta> hijos;
        public Carpeta(String nombre) {
            this.nombre = nombre;
            hijos = new LinkedList<Carpeta>();
        }
    }

    private Carpeta root;

    public ArbolDirectorio() {
        root = new Carpeta("root");
    }

    /*
     * Dado un path del formato 'root/carpeta_1/.../carpeta_n', retornar el objeto CarpetaN
     * En caso de no encontrarlo, retornar Null
     */
    private Carpeta ir(String path) {
        StringTokenizer stPath = new StringTokenizer(path,"/");
        String nombreCarpeta;
        if(stPath.countTokens() == 1) return root;
        stPath.nextToken();
        Carpeta c = root;
        boolean encontrado = false;
        while(stPath.hasMoreTokens()) {
            nombreCarpeta = stPath.nextToken();
            encontrado = false;
            for(Carpeta h : c.hijos) {
                if(h.nombre.equals(nombreCarpeta)) {
                    c = h;
                    encontrado = true;
                    break;
                }
            }
            if(!encontrado) return null;
        }
        return c;
    }

    /*
     * Retornar la cantidad total de carpetas que posee el �rbol de directorios
     */
    public int size() {
        //Almacena la carpeta raiz
        Carpeta x = root;
        int cant = 1;
        //Si no tiene hijos solo devuelve 1
        if(root.hijos.size() == 0){
            return 1;
        }else{
            //Aumentara el tamaño segun cuantos hijos tenga el hijo de la carpeta
            for(Carpeta c : root.hijos){
                cant+=(1+c.hijos.size());
            }
            return cant;
        }

    }

    /*
     * Dado un path del formato 'root/carpeta_1/.../carpeta_n' y un nombre de nueva carpeta, crear una nueva carpeta al
     * interior de la CarpetaN y retornar true
     * Si no se consigue, retornar false
     * CONDICION: no pueden haber dos carpetas en el mismo directorio con el mismo nombre
     */
    public boolean crearCarpeta(String path, String nombre) {
        //Si el path esta vacio, comprobara si la raiz es nula para crearla
        //Sino cancelara la creacion
        if (path == "") {
            if(root!=null){
                root = new Carpeta(nombre);
                return true;
            }else{
                return false;
            }
        }else{
            //Almacena cada carpeta del camino en busca de ubicar donde creara la nueva
            StringTokenizer st = new StringTokenizer(path,"/");
            Carpeta nodo = root;
            while (st.hasMoreTokens()){
                String token = st.nextToken();
                for(Carpeta x : nodo.hijos){
                    if(x.nombre.equalsIgnoreCase(token)){
                        nodo = x;
                        break;
                    }
                }
            }
            //Antes de crear la carpeta prueba si existe una con nombre igual
            //En caso de que no exista la creara, sino cancela el metodo
            if(!isCarpeta(nodo.hijos,nombre)){
                nodo.hijos.add(new Carpeta(nombre));
                return true;
            }else{
                return false;
            }
        }
    }

    /*
     * Mover la carpeta a la que apunta path1 ('root/carpeta_1/.../carpeta_n') al interior de la carpeta apuntada por
     * path2 ('root/carpeta_1/.../carpeta_m'), de tal forma que las subcarpetas de carpetaN sean movidas como subcarpetas de
     * CarpetaM.
     * En caso de conseguirlo, retornar true, si no, retornar false
     * CONDICION: no pueden haber dos carpetas en el mismo directorio con el mismo nombre.
     * Si se da el caso, cancele toda la operaci�n
     */
    public boolean mover(String path1, String path2) {
        //Retorna falso en caso de los path sean vacios
        if(path1=="" || path2==""){
            return false;
        }else{
            Carpeta objetivo = ir(path1);//Busca la carpeta en path1
            Carpeta ubicacion = ir(path2);//Busca la carpeta en path2
            //Comprueba el caso de que exista una carpeta en ubicacion con el nombre de algun hijo del objetivo
            if(objetivo!=null || ubicacion!=null){
                for(Carpeta x : objetivo.hijos){
                    if(isCarpeta(ubicacion.hijos, x.nombre)){
                        return false;
                    }
                }
            }
            //Si no hay una carpeta de mismo nombre, añade cada una
            for (Carpeta y : objetivo.hijos){
                ubicacion.hijos.add(y);
            }
            //limpia los hijos del objetivo
            objetivo.hijos.clear();
            return true;
        }

    }

    /*
     * Dada una lista de hermanos, averiguar si en su interior se encuentra una con el nombre entregado como par�metro
     */
    private boolean isCarpeta(LinkedList<Carpeta> hermanos, String nombre) {
        for(Carpeta h : hermanos) {
            if(h.nombre.equals(nombre)) return true;
        }
        return false;
    }

    /*
     * Eliminar la carpeta carpetaN ubicada en path ('root/carpeta_1/.../carpeta_n') junto a todo su subarbol
     * Usted elige la politica de eliminaci�n del nodo raiz
     */
    public boolean eliminarCarpeta(String path) {
        //En el caso de que el path sea nulo, retorna false
        if(path == ""){
            return false;
        }else{
            Carpeta x = ir(path);//Almacena la carpeta ubicada en el path
            String[] carpetas = path.split("/");
            Carpeta raiz = root;
            //En el caso de que la raiz contenga a la carpeta, eliminara directamente la carpeta
            if(root.hijos.contains(x)){
                raiz.hijos.remove(x);
                return true;
            }else{
                //Si la raiz no contiene la carpeta directamente, buscara el padre de la carpeta a eliminar
                for(int i=0; i<carpetas.length-1;i++){
                    for(Carpeta c : root.hijos){
                        if(c.nombre.equalsIgnoreCase(carpetas[i])){
                            raiz = c;
                            break;
                        }
                    }
                }
                if(raiz == root){
                    return false;
                }
                //Eliminara la carpeta una vez termine de encontrar su padre
                raiz.hijos.remove(x);
                return true;
            }


        }
    }

    public void print() {
        System.out.println("==================");
        System.out.println(root.nombre);
        print(root, 1);
        System.out.println("==================");
    }

    private void print(Carpeta c, int nivel) {
        String l = "";
        for(int i = 0; i < nivel; i++) l += "-";

        for(Carpeta h : c.hijos) {
            System.out.println(l+h.nombre);
            print(h, nivel+1);
        }
    }

}
