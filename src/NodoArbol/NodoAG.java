package NodoArbol;

public class NodoAG {
    String nombre;
    NodoAG hijo;
    NodoAG hermano;
    NodoAG(String name, NodoAG hijo, NodoAG bro){
        nombre=name;
        this.hijo=hijo;
        hermano=bro;
    }

}
