package NodoArbol;

import java.util.StringTokenizer;

public class ArbolGeneral {
    private NodoAG root;

    public ArbolGeneral(){
        root = null;
    }

    public boolean add(String nombre, String camino){
        //ej. "a/b/c"
        // ejemplo de camino para insertar en raiz ""
        if (camino == "") {
            if(root!=null){
                root = new NodoAG(nombre,null,null);
                return true;
            }else{
                return false;
            }
        }else{
            StringTokenizer st = new StringTokenizer(camino,"/");
            NodoAG nodo = root;
            NodoAG ant = null;
            while (st.hasMoreTokens()){
                String token = st.nextToken();
                if(nodo.nombre.equalsIgnoreCase(token)){
                    ant=nodo;
                    nodo=nodo.hijo;
                }else{
                    do {
                        nodo = nodo.hermano;
                    }while(nodo!=null && nodo.nombre.equalsIgnoreCase(token));
                    if(nodo == null){
                        return false;
                    }
                    ant=nodo;
                    nodo=nodo.hijo;
                }
            }
            NodoAG nuevo = new NodoAG(nombre, null, ant.hijo);
            ant.hijo=nuevo;
            return true;
        }


    }
    public void preOrden(){
        preOrdenRec(root);
    }

    public void preOrdenRec(NodoAG raiz){
        if(raiz!=null){
            System.out.println(raiz.nombre);
            NodoAG n = raiz.hijo;
            while(n!=null){
                preOrdenRec(n);
                n=n.hermano;
            }

        }
    }
}
