package BSTvsAVL;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TestMain {

	public static void main(String[] args) {
		/*
		 * Escriba aqu� su c�digo
		 */
		AvlTree x = new AvlTree();//arbol AVL de prueba
		BSTree y = new BSTree();//Arbol BST de prueba
		List<Integer> datosTree = new ArrayList<>();//Lista de datos para busqueda exitosa e insercion
		List<Integer> datosTree1 = new ArrayList<>();//Lista de datos para busqueda fallida
		long[][] times = new long[3][2];//Matriz para almacenamiento de resultados
		long start = 0;
		long finish = 0;
		for(int i=0; i<100000; i++){
			datosTree.add((i*2));
			datosTree1.add((i*2)+1);
		}
		Collections.shuffle(datosTree);
		Collections.shuffle(datosTree1);
		//Pruebas de insertar 100000 datos
		start = System.nanoTime();
		for(int i=0; i<datosTree.size(); i++){
			x.insert(datosTree.get(i));
		}
		finish = System.nanoTime() - start;
		times[0][0] = finish;
		start = System.nanoTime();
		for(int i=0; i<datosTree.size(); i++){
			y.insert(datosTree.get(i));
		}
		finish = System.nanoTime() - start;
		times[0][1] = finish;

		//Pruebas de buscar 100000 datos insertados
		start = System.nanoTime();
		for(int i=0; i<datosTree.size(); i++){
			x.search(datosTree.get(i));
		}
		finish = System.nanoTime() - start;
		times[1][0] = finish;
		start = System.nanoTime();
		for(int i=0; i<datosTree.size(); i++){
			y.search(datosTree.get(i));
		}
		finish = System.nanoTime() - start;
		times[1][1] = finish;

		//Pruebas de buscar 100000 datos no insertados
		start = System.nanoTime();
		for(int i=0; i<datosTree1.size(); i++){
			x.search(datosTree1.get(i));
		}
		finish = System.nanoTime() - start;
		times[2][0] = finish;
		start = System.nanoTime();
		for(int i=0; i<datosTree1.size(); i++){
			y.search(datosTree1.get(i));
		}
		finish = System.nanoTime() - start;
		times[2][1] = finish;

		System.out.println("Tiempos de insercion = ");
		System.out.println("AVL:" + times[0][0] + " BST:" + times[0][1]);
		System.out.println("Tiempos de busqueda = ");
		System.out.println("AVL:" + times[1][0] + " BST:" + times[1][1]);
		System.out.println("Tiempos de busqueda fallida = ");
		System.out.println("AVL:" + times[2][0] + " BST:" + times[2][1]);
		/*
		Cuando la lista de objetos es ordenada, el AVL es mas rapida en los tres casos de prueba.
		La razon de que el AVL sea mas rapido se debe a que a diferencia de cuando el metodo tiene una lista
		desordenada, esta no requiere de demasiado tiempo en balancear el arbol, al solo tener que moverlo en una
		direccion, haciendolo mas rapido con un orden O(n log n).
		Cuando la lista de objetos esta desordenada, el BST es mas rapido a la hora de insertar los datos.
		Esto debido a tiempo que demora el AVL al balancear el arbol cada vez que se desbalancea.
		En los casos de busqueda, el AVL es mas rapido, al tener un O(n log n), mientras que el BST en el peor
		caso solo tiene de altura O(n).

		*/

	}

}
